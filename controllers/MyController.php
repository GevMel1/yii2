<?php

namespace app\controllers;

use app\models\ContactForm;
use app\models\MailForm;
use Yii;
use yii\base\Controller;

class MyController extends Controller
{
    public function actionIndex($fio){

        return $this->render('index', [
            'fio' => $fio,
        ]);
    }

    public function actionMail()
    {
        $model = new MailForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }

            return $this->render('mail', [
                'model' => $model,
            ]);
    }

    public function actionI()
    {
        return $this->render('i', [
            'name' => 'Gevorg',
            'age' => 18,
            'addres' => 'ul Pushkina, dom Kolotushkina'
        ]);

    }

    public function actionOther()
    {
        return $this->render('other', [
            'name' => 'Pasha',
            'age' => 19,
            'addres' => 'ul Kolotushkina, dom Pushkina',
            'arr' => [1, 2, 3]
        ]);

    }
}