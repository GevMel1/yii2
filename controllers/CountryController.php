<?php

namespace app\controllers;

use app\models\Country;
use app\models\CountryForm;
use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\EntryForm;


class CountryController extends Controller
{

    public function actionEntry()
    {
        $model = new CountryForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // данные в $model удачно проверены

            // делаем что-то полезное с $model ...

            return $this->render('entry-confirm', ['models' => $model]);
        } else {
            // либо страница отображается первый раз, либо есть ошибка в данных
            return $this->render('entry', ['models' => $model]);
        }
    }

    public function actionList()
    {
        $models = Country::find()->all();
        return $this->render('list', [
            'models' => $models,
        ]);
    }

    public function actionNew()
    {
        $model = new CountryForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->refresh();
        }
        return $this->render('new', [
            'model' => $model
        ]);
    }

    public function actionIndex()
    {

//        $models = Country::find()
//            ->all();
//        foreach ($models as $model) {
//            echo $model->code . '/' . $model->name . '/' . $model->population . '<br>';
//        }
//        var_dump($models);
//        die();
//
//        $query = Country::find();
//
//        $pagination = new Pagination([
//            'defaultPageSize' => 6,
//            'totalCount' => $query->count(),
//        ]);
//
//        $countries = $query->orderBy('name')
//            ->offset($pagination->offset)
//            ->limit($pagination->limit)
//            ->all();
//
        return $this->render('index', []);
    }
}