<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%country}}`.
 */
class m231016_060915_add_position_column_to_country_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%country}}', 'status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%country}}', 'status');
    }
}
