<?php

use yii\helpers\Html;

?>
<p>Вы ввели следующую информацию:</p>

<ul>
    <li><label>Name</label>: <?= Html::encode($models->name) ?></li>
    <li><label>Email</label>: <?= Html::encode($models->email) ?></li>
</ul>