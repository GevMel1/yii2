<?php

/** @var yii\web\View $this */
/** @var string $fio */


$this->title = 'My Yii Application';
?>

<div class="jumbotron text-center bg-transparent mt-5 mb-5">
    <h1 class="display-4"><?="My name is $fio"?></h1>
</div>