<?php

/** @var yii\web\View $this */
/** @var string $name */
/** @var string $age */
/** @var string $addres */


$this->title = 'My Yii Application';
?>

<div class="jumbotron text-center bg-transparent mt-5 mb-5">
    <h1 class="display-4"><?="My name is $name"?></h1>
    <h1 class="display-4"><?="I am $age"?></h1>
    <p class="display-4"><?="My addres is $addres"?></p>
</div>